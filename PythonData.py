import csv
from matplotlib import pyplot as plt
from textwrap import wrap
import operator

def parse_data():

    match_per_year = {}
    match_winner = {}
    extra_run_in_2016 = {}
    id_2015_list = []
    id_2016_list = []
    bowler_economy_data = {}
    bowler_economy = {}

    with open('./data/matches.csv') as csvfile:
        reader = csv.DictReader(csvfile)

        for i in reader:

            id = int(i['id'])
            year = int(i['season'])
            winner = i['winner']

            if year in match_per_year and year:
                match_per_year[year]+=1
            else:
                match_per_year[year] = 1

            if year in match_winner:
                if winner in match_winner[year] and winner:
                    match_winner[year][winner]+=1
                elif winner:
                    match_winner[year][winner] = 1
            else:
                match_winner[year] = {}

            if year == 2015:
                id_2015_list.append(int(id))
            if year == 2016:
                id_2016_list.append(int(id))

        min_id_15 = min(id_2015_list)
        max_id_15 = max(id_2015_list)

        min_id_16 = min(id_2016_list)
        max_id_16 = max(id_2016_list)

    with open('./data/deliveries.csv') as csvfile:
        reader = csv.DictReader(csvfile)

        for i in reader:

            match_id = int(i['match_id'])
            if match_id >= min_id_15 and match_id <= max_id_15:
                bowler = i['bowler']
                total_run = int(i['total_runs'])

                if bowler in bowler_economy_data and bowler:
                    bowler_economy_data[bowler]['run'] += total_run
                    bowler_economy_data[bowler]['bowl'] += 1
                elif bowler:
                    bowler_economy_data[bowler] = {'run': total_run, 'bowl': 1}

            if match_id >= min_id_16 and match_id <= max_id_16:
                bowling_team = i['bowling_team']
                extra_run = int(i['extra_runs'])

                if bowling_team in extra_run_in_2016 and bowling_team:
                    extra_run_in_2016[bowling_team] += extra_run
                elif bowling_team:
                    extra_run_in_2016[bowling_team] = extra_run
    for key in bowler_economy_data:

        economy = round((bowler_economy_data[key]['run']/bowler_economy_data[key]['bowl'])*6, 2)
        bowler_economy[key] = economy

    return match_per_year, match_winner, extra_run_in_2016, bowler_economy


def plot(data, x_axis_name, y_axis_name, title):

    x_axis, y_axis = [], []
    for key in sorted(data):
        x_axis.append(key)
        y_axis.append(data[key])

    x_axis_list = range(len(x_axis))
    plt.bar(x_axis_list,y_axis)

    plt.xlabel(x_axis_name, color='g')
    plt.ylabel(y_axis_name, color='c')

    team = ['\n'.join(wrap(str(name), 10)) for name in x_axis]
    plt.xticks(x_axis_list, team)
    plt.title(title)
    plt.show()
    """
    1. Plot the number of matches played per year of all the years in IPL.
    2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
    3. For the year 2016 plot the extra runs conceded per team.
    4. For the year 2015 plot the top economical bowlers.
    5. Discuss a "Story" you want to tell with the given data. As with part 1, prepare the data structure and plot with matplotlib.
    """

def show():
    match_per_year, match_winner, extra_run_in_2016, bowler_economy = parse_data()
    economy = sorted(bowler_economy.items(), key=operator.itemgetter(1))
    top_economy = {}
    for item in economy[0:5]:
        top_economy[item[0]] = item[1]


    plot(data=match_per_year, x_axis_name='year',y_axis_name='number', title='Matches in IPL-Season')
    plot(data=extra_run_in_2016, x_axis_name='Team', y_axis_name='number', title='Extra Run\'s in IPL-Season')
    plot(data=top_economy, x_axis_name='Bowler',y_axis_name='Economy', title='Top-Economy in IPL-Season')
    plot_stacked(match_winner)

def plot_stacked(data):

    ipl_year = sorted(data.keys())
    all_teams = []

    for team_win in data.values():
        all_teams.extend(team_win.keys())

    all_teams = sorted((set(all_teams)))

    team_all_wins = []
    ind_team = range(len(all_teams))
    ind_year = range(len(ipl_year))
    for team in all_teams:
        team_wins = []
        for year in ipl_year:
            if team in data[year].keys():
                team_wins.append(data[year][team])
            else:
                team_wins.append(0)

        team_all_wins.append({team:team_wins})

    color = {
        'Chennai Super Kings': 'aqua',
        'Deccan Chargers':'chartreuse',
        'Delhi Daredevils': 'gray',
        'Gujarat Lions': 'yellow',
        'Kings XI Punjab': 'red',
        'Kochi Tuskers Kerala': 'coral',
        'Kolkata Knight Riders': 'fuchsia',
        'Mumbai Indians': 'gold',
        'Pune Warriors': 'green',
        'Rajasthan Royals': 'lime',
        'Rising Pune Supergiant':'lavender',
        'Rising Pune Supergiants': 'teal',
        'Royal Challengers Bangalore': 'tan',
        'Sunrisers Hyderabad': 'salmon'
    }
    bottom_flag = False
    bar2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for item in team_all_wins:

        for key, val in item.items():
            if bottom_flag:
                bottom = list(map(lambda x, y: x + y, bar1, bar2))
                bar2 = bottom
                plt.bar(ind_year, val, bottom=bottom,color = color[key], width=0.5)
            else:
                plt.bar(ind_year, val, color=color[key], width=0.5)

            bottom_flag = True
            bar1 = val
    plt.grid()
    # plt.legend(color)
    plt.xticks(ind_year, ipl_year)
    # plt.yticks(val, all_teams)
    plt.show()

show()